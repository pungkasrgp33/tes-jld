<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">

        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav"  style="margin-left:375px">
                

                <?php if(isset($_SESSION['level']) && $_SESSION['level']==1) { ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Master Data <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="?page=obat&actions=tampil">Data Obat</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="?page=laporanobat&actions=report">Laporan Obat</a></li>
                    </ul>
                </li>
                <li><a href="?page=user&actions=tampil">User</a></li>
                <?php } ?>

                    <?php if (isset($_SESSION['username'])) { ?>
                    <li><a href="logout.php">Logout</a></li>
                <?php } ?>

            </ul>
        </div>
    </div>
</nav>
