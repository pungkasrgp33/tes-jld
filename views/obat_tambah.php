<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Form Tambah Data Obat</h3>
                </div>
                <div class="panel-body">
                    <!--membuat form untuk tambah data-->
                    <form class="form-horizontal" action="" method="post">
						 <div class="form-group">
                            <label for="id_obat" class="col-sm-3 control-label">ID Obat</label>
                            <div class="col-sm-9">
                                <input type="text" name="id_obat" class="form-control" id="inputEmail3" placeholder="Inputkan ID Obat" required>
                            </div>
                        </div>
						 <div class="form-group">
                            <label for="nama_obat" class="col-sm-3 control-label">Nama Obat</label>
                            <div class="col-sm-9">
                                <input type="text" name="nama_obat" class="form-control" id="inputEmail3" placeholder="Inputkan Nama Obat" required>
                            </div>
                        </div>
						 <div class="form-group">
                            <label for="jenis_obat" class="col-sm-3 control-label">Jenis Obat</label>
                            <div class="col-sm-9">
                                <input type="text" name="jenis_obat" class="form-control" id="inputEmail3" placeholder="Inputkan Jenis Obat" required>
                            </div>
                        </div>
						 <div class="form-group">
                            <label for="stok" class="col-sm-3 control-label">Stok</label>
                            <div class="col-sm-9">
                                <input type="text" name="stok" class="form-control" id="inputEmail3" placeholder="Inputkan Stok" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tgl_dibuat" class="col-sm-3 control-label">Tanggal Dibuat</label>
                            <div class="col-sm-9">
                                <input type="date" name="tgl_dibuat"class="form-control" id="inputEmail3" placeholder="Inputkan Tanggal Dibuat" required>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="tgl_kadaluarsa" class="col-sm-3 control-label">Tanggal Kadaluarsa</label>
                            <div class="col-sm-9">
                                <input type="date" name="tgl_kadaluarsa" class="form-control" id="inputEmail3" placeholder="Inputkan Tanggal Kadaluarsa" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="created_by" class="col-sm-3 control-label">Created By</label>
                            <div class="col-sm-9">
                                <input type="text" name="created_by" class="form-control" id="inputPassword3" placeholder="Inputkan Created By" required>
                            </div>
                        </div>

                        <!--Status-->
                        <!--Akhir Status-->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-success">
                                    <span class="fa fa-save"></span> Simpan Data Obat</button>
                            </div>
                        </div>
                    </form>


                </div>
                <div class="panel-footer">
                    <a href="?page=obat&actions=tampil" class="btn btn-danger btn-sm">
                        Kembali Ke Data Obat
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>

<?php
if($_POST){
    //Ambil data dari form
	$id_obat=$_POST['id_obat'];
	$nama_obat=$_POST['nama_obat'];
	$jenis_obat=$_POST['jenis_obat'];
    $stok=$_POST['stok'];
	$tgl_dibuat=$_POST['tgl_dibuat'];
    $tgl_kadaluarsa=$_POST['tgl_kadaluarsa'];
    $created_by=$_POST['created_by'];
    //buat sql
    $sql="INSERT INTO obat VALUES ('','$id_obat','$nama_obat','$jenis_obat','$stok','$tgl_dibuat','$tgl_kadaluarsa','$created_by')";
    $query=  mysqli_query($koneksi, $sql) or die ("SQL Simpan Arsip Error");
    if ($query){
        echo "<script>window.location.assign('?page=obat&actions=tampil');</script>";
    }else{
        echo "<script>alert('Simpan Data Gagal');<script>";
    }
    }

?>
